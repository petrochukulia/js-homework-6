function Human(name, age) {
	this.age = age,
		this.name = name
}

let family = [];
family[0] = new Human(prompt("Введіть Ваше ім'я"), parseInt(prompt("Введіть Ваш вік")));
family[1] = new Human(prompt("Введіть ім'я вашої мами"), parseInt(prompt("Введіть її вік")));
family[2] = new Human(prompt("Введіть ім'я вашого батька"), parseInt(prompt("Введіть його вік")));

document.getElementById("user").innerHTML = `Ваше ім'я: ${family[0].name}, ваш вік: ${family[0].age}.`;
document.getElementById("mom").innerHTML = `Ім'я вашої мами: ${family[1].name}, її вік: ${family[1].age}.`;
document.getElementById("dad").innerHTML = `Ім'я вашого батька: ${family[2].name}, його вік: ${family[2].age}.`;


function sortByAge(arr) {
	arr.sort((a, b) => a.age > b.age ? 1 : -1);
}
function sortByAgeReverse(arr) {
	arr.sort((a, b) => a.age > b.age ? -1 : 1);
}

function print(mas) {
	for (let i = 0; i < mas.length; i++) {

		(document.write(`${mas[i].name}:${mas[i].age} <br>`))
	}
}
function oldest(mas) {
	let maxAge = Number.NEGATIVE_INFINITY;
	for (i = 0; i < mas.length; i++) {
		if (mas[i].age > maxAge) {
			maxAge = mas[i].age;
		}
		return maxAge;
	}
}

sortByAge(family);
document.write(`<br>`)
document.write(`Сортування членів родини в порядку збільшення віку:  <br>`)
print(family);

sortByAgeReverse(family);
document.write(`<br>`)
document.write(`Сортування членів родини в порядку зменшення віку:  <br>`)
print(family);

document.write(`<br>`)
document.write(`Найсатршому члену родини: ${oldest(family)}`);